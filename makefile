all: clean
	arm-none-eabi-gcc -mcpu=cortex-a7 -fpic -ffreestanding -c src/boot.s -o boot.o
	xargo build --target arm-none-eabihf
	arm-none-eabi-gcc -T linker.ld -o myos.elf -ffreestanding -O2 -nostdlib boot.o target/arm-none-eabihf/debug/librpi_kernel.rlib

clean:
	xargo clean
	rm -f boot.o
	rm -f myos.elf

run: all
	qemu-system-arm -M raspi2 -kernel myos.elf -serial stdio -display none
