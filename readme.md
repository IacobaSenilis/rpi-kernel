# rpi-kernel

This is a minimalist proof of concept kernel for the raspberry pi 2.

## Dependencies
You need a rust compiler installed, I recommend rustup for managing compilers (install this through your package manager)
Then you need to install rust-src and xargo:
`rustup component add rust-src`
`cargo install xargo`
You also need an arm-eabi gcc cross compiler, for arch install the arm-none-eabi-gcc package, for gentoo you can use [crossdev](https://wiki.gentoo.org/wiki/Crossdev), for other refer to [this](https://wiki.osdev.org/GCC_Cross-Compiler).

In order to run you will need qemu-system-arm, on arch you can install qemu-arch-extra to get this.

## Running
`make run`

## What will Happen?
It should print "Hello world!" to the serial (represented here as stdout) and then hang.

## License
[GPL](https://www.gnu.org/licenses/gpl-3.0.en.html)

## Resources
Thanks to the following resources:
https://rust-embedded.github.io/book/intro/index.html
https://os.phil-opp.com/
https://wiki.osdev.org/Raspberry_Pi_Bare_Bones
https://wiki.osdev.org/GCC_Cross-Compiler
